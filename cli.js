'use strict';
var com = require('./lib/com');
var unfnd = require('./lib/unfnd');
var sort = require('./lib/sort');
var prompt = require('prompt');

prompt.message = "";
prompt.delimiter = "";
prompt.start();


var start = function(){
  console.log('1)Union find \n2)Sort \n0)Exit');
  prompt.get({name: 'option', description: '>'},
    function(err, res){
      if (err) throw err;
      if (res.option == 0) console.log('Bye');
      if (res.option == 1) menu('unfnd');
      if (res.option == 2) menu('sort');
    }
  );
}

var menu = function(part){
  if (part == 'sort') {
    console.log('[n]ew array, [c]heck array, [s]ort or [e]xit?');
    prompt.get({name: 'option', description: '>'},
    function(err, res){
      if (err) throw err;
      switch (res.option){
        case 'n':
          prompt.get([
            {name: 'length', description: 'length of array?'},
            {name: 'biggest', description: 'biggest item?'}
            ],
            function(err, res){
              if (err) throw err;
              var array = com.createRandomArray(res.length, res.biggest);
              com.writeFile(sort.fileName(), array);
              menu('sort');
            }
          );
          break;
        case 's':
          console.log('1 Selection\n2 Insertion\n3 Shell\n0 Shuffle');
          prompt.get([
            {name: 'option', description: '>'}
            ],
            function(err, res){
              if (res.option == 1) sort.selectionSort();
              if (res.option == 2) sort.insertionSort();
              if (res.option == 3) sort.shellSort();
              if (res.option == 0) sort.shuffle();
              menu('sort');
            }
          );
          break;
        case 'c':
          sort.check();
          menu('sort');
          break;
        case 'e':
          console.log('Bye');
          break;
        default:
          console.log('Wrong input, try again, pls');
          menu('sort');
        }
      }
    );
  }
  if (part == 'unfnd') {
    console.log('[n]ew array, [u]nion, [f]ind or [e]xit?');
    prompt.get({name: 'option', description: '>'},
    function(err, res){
      if (err) throw err;
      switch (res.option){
        case 'n':
          console.log('length of array?');
          prompt.get({name: 'length'},
          function(err, res){
            if (err) throw err;
            com.writeFile(unfnd.fileName(), com.createArray(res.length));
            menu('unfnd');
          }
        );
        break;
        case 'u':
          console.log('Index numbers of units to union');
          prompt.get([
            {message: '1st item', name: 'u1'},
            {message: '2nd item', name: 'u2'}
            ], function(err, res){
              if (err) throw err;
              unfnd.union(res.u1, res.u2);
              menu('unfnd');
            }
          );
          break;
        case 'f':
          console.log('Index numbers of units to find');
          prompt.get([
            {message: '1st item', name: 'u1'},
            {message: '2nd item', name: 'u2'}
            ], function(err, res){
              if (err) throw err;
              unfnd.find(res.u1, res.u2);
              menu('unfnd');
            }
          );
          break;
        case 'e':
          console.log('Bye');
          break;
        default:
          console.log('Wrong input, try again, pls');
          menu('unfnd');
        }
      }
    );
  };

}

start();
