'use strict';
var com = require('./lib/com');
var sort = require('./lib/sort');
var express = require('express');
var app = express();
var handlebars = require('express-handlebars');
var bodyParser = require('body-parser')

app.engine('hbs', handlebars({extname:'hbs', defaultLayout:'main.hbs'}));
app.set('view engine', 'hbs');
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({extended: true}));

app.get('/', function (req, res) {
  res.render('index');
});
app.get('/sort', function (req, res) {
  var array = com.readFile('data/sort_array');
  res.render('sort', {array:array});
});
app.get('/newRandomArray', function(req, res){
  com.writeFile('data/sort_array', com.createRandomArray(10, 9));
  var array = com.readFile('data/sort_array');
  res.render('sort', {array:array});
});
app.get('/newArray', function(req, res){
  com.writeFile('data/sort_array', com.createArray(10));
  var array = com.readFile('data/sort_array');
  res.render('sort', {array:array});
});
app.get('/selectionSort', function(req, res){
  sort.selectionSort();
  var array = com.readFile('data/sort_array');
  res.render('sort', {array:array});
});
app.get('/insertionSort', function(req, res){
  sort.insertionSort();
  var array = com.readFile('data/sort_array');
  res.render('sort', {array:array});
});
app.get('/shellSort', function(req, res){
  sort.shellSort();
  var array = com.readFile('data/sort_array');
  res.render('sort', {array:array});
});
app.get('/shuffle', function(req, res){
  sort.shuffle();
  var array = com.readFile('data/sort_array');
  res.render('sort', {array:array});
});

//server
var server = app.listen(3000, function () {
  var host = server.address().address
  var port = server.address().port
  console.log('The app listening at http://%s:%s', host, port)
});
