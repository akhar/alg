// common goodies

'use strict';
var fs = require('fs');

module.exports = {
  createArray: function(length){
    var array =[];
    for (var i=0; i<length; i++) {
      array.push(i);
    }
    return array;
  },

  createRandomArray: function(length, maxItem){
    var array = [];
    for (var i=0; i<length; i++) {
      array.push(Math.round(Math.random()*maxItem));
    }
    return array;
  },

  randomNumber: function(maximum){
    return Math.round(Math.random()*maximum);
  },

  readFile: function(fileName){
    var data = fs.readFileSync(fileName, 'utf-8');
    data = data.split(",");
    // serealize strings to numbers
    for (var i=0; i<data.length; i++) {
      data[i] = +data[i];
    }
    return data;
  },

  writeFile: function(fileName, data){
    fs.writeFileSync(fileName, data);
    console.log(fileName, ' saved!');
  }
};
