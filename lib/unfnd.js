//Union-find

'use strict';
var com = require('./com');
var fileName = 'data/unfnd_array';

module.exports = {
  fileName: function(){
    return fileName;
  },
  union: function(u1, u2){
    var array = com.readFile(fileName);
    var root1 = this.findRoot(u1);
    var root2 = this.findRoot(u2);
    if (root1.hight >= root2.hight){
      array[root1.root] = Number(root2.root);
    } else {
      array[root2.root] = Number(root1.root);
    }
    com.writeFile(fileName, array);
    console.log('union done');
  },

  findRoot: function(item){
    var array = com.readFile(fileName);
    var counter = 0;
    var _item = item;
    while (array[item] != item) {
      item = array[item];
      counter++
    }
    array[_item] = item;
    return {root: item, hight: counter}
  },

  find: function(u1, u2){
    var root1 = this.findRoot(u1);
    var root2 = this.findRoot(u2);
    if (root1.root == root2.root) {
      console.log('true');
    } else {
      console.log('false');
    }
  }
};
