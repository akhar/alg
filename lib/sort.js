'use strict';
var com = require('./com');
var fileName = 'data/sort_array';

module.exports = {
  fileName: function(){
    return fileName;
  },

  shuffle: function(){
    //Knuth shuffle
    var array = com.readFile(fileName);
    for(var i=0; i<array.length; i++){
      var t = array[i];
      var p = com.randomNumber(i)
      array[i] = array[p];
      array[p] = t;
    }
    com.writeFile(fileName, array);
    console.log('Shuffle done');
  },

  shellSort: function(){
    var array = com.readFile(fileName);

    // Marcin Ciura's gap sequence
    var ciura = [1, 4, 10, 23, 57, 132, 301, 701, 1750];
    if (ciura[ciura.length-1] < array.length){
      while (ciura[ciura.length-1] < array.length){
        ciura.push(Math.floor(ciura[ciura.length-1]*2.2));
      }
    }
    for(var l=ciura.length; l>0; l--){
      if (ciura[l] > array.length) ciura.pop();
    }

    for(var k=ciura.length; k>=0; k--){
      var n = ciura[k];
      for (var i=n; i<array.length-n+1; i+=n){
        var j=i;
        while (array[j] < array[j-n]){
          var t = array[j-n];
          array[j-n] = array[j];
          array[j] = t;
          j-=n;
        }
      }
    }
    com.writeFile(fileName, array);
    console.log('Shell sort done');
  },

  insertionSort: function(){
    var array = com.readFile(fileName);
    for(var i=1; i<array.length; i++){
      var j=i;
      while (array[j] < array[j-1]){
        var t = array[j-1];
        array[j-1] = array[j];
        array[j] = t;
        j--;
      }
    }
    com.writeFile(fileName, array);
    console.log('insertion sort done');
  },

  selectionSort: function(){
    var array = com.readFile(fileName);
    var length = array.length;
    for(var i=0; i<length; i++){
      //find for minimum
      var m = i;
      for(var j=i; j<length; j++){
        if (array[j] <= array[m]) m = j;
      }
      //switch minimum and first in remainder
      var t = array[i];
      array[i] = array[m];
      array[m] = t;
    }
    com.writeFile(fileName, array);
    console.log('selection sort done');
  },

  check: function(){
    var array = com.readFile(fileName);
    for(var i=0; i<array.length; i++) {
      if (array[i] > array[i+1]){
        console.log('check failed');
        return false;
      }
    }
    console.log('check passed');
    return true;
  }
};
